//
//  Planets.swift
//  StarWarsLabb
//
//  Created by Fredrik Bredenius on 2017-11-22.
//  Copyright © 2017 Fredrik Bredenius. All rights reserved.
//

import Foundation


public class Planet{
    var name: String!
    var diameter: String!
    var population: String!
}
