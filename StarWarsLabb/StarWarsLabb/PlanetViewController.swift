//
//  PlanetViewController.swift
//  StarWarsLabb
//
//  Created by Fredrik Bredenius on 2017-11-22.
//  Copyright © 2017 Fredrik Bredenius. All rights reserved.
//

import UIKit

class PlanetViewController: UIViewController {
    
    var data:Planet? = nil
    
    @IBOutlet weak var planetNameLabel: UILabel!
    @IBOutlet weak var planetPopulationLabel: UILabel!
    @IBOutlet weak var planetDiameterLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        planetNameLabel.text = ("Name: " + data!.name)
        planetPopulationLabel.text = ("Population: " + data!.population)
        planetDiameterLabel.text = ("Diameter: " + data!.diameter)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
