//
//  TableViewCell.swift
//  StarWarsLabb
//
//  Created by Fredrik Bredenius on 2017-11-22.
//  Copyright © 2017 Fredrik Bredenius. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var cellLabelName: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
