//
//  ViewController.swift
//  StarWarsLabb
//
//  Created by Fredrik Bredenius on 2017-11-22.
//  Copyright © 2017 Fredrik Bredenius. All rights reserved.
//

import UIKit

class TableViewController: UITableViewController {
    
    
    
    @IBOutlet var planetsTableView: UITableView!
    public var planetArray: [Planet] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        planetsTableView.dataSource = self
        getPlanetsData()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func getPlanetsData(){
        let url = URL(string: "https://swapi.co/api/planets")
        let task = URLSession.shared.dataTask(with: url!) { (data, response, error) in
            if error != nil {
                print("ERROR")
            }
            else
            {
                if let content = data
                {
                    do
                    {
                        let jsonData = try JSONSerialization.jsonObject(with: content, options: JSONSerialization.ReadingOptions.mutableContainers) as AnyObject
                        DispatchQueue.main.async {
                            if let planets = jsonData["results"] as? NSArray
                            {
                                for dics in planets {
                                    let data = dics as! NSDictionary
                                    let parsedPlanet = Planet()
                                    parsedPlanet.diameter = data["diameter"] as! String
                                    parsedPlanet.name = data["name"] as! String
                                    parsedPlanet.population = data["population"] as! String
                                    self.planetArray.append(parsedPlanet)
                                }
                            }
                            self.planetsTableView.reloadData()
                        }
                    }
                    catch
                    {
                        
                    }
                }
            }
            
        }
        
        task.resume()
        self.tableView.reloadData()
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return planetArray.count
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = planetsTableView.dequeueReusableCell(withIdentifier: "PlanetsCell", for: indexPath) as! TableViewCell
        cell.cellLabelName.text = planetArray[indexPath.row].name
        print(planetArray[indexPath.row])
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        var planet = Planet()
        planet = planetArray[indexPath.row]
        performSegue(withIdentifier: "PlanetSegue", sender: planet)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let planetController = segue.destination as? PlanetViewController{
            planetController.data = sender as! Planet
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

